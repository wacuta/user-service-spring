# User Service

> This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

## Run

The service can be launch with [Docker](https://www.docker.com/).

```
$ docker-compose up --build
```

### Docker containers

This project is composed of 2 containers:
 - **app**: The web service in Java Spring Boot
 - **db**: The NoSQL database name Redis

### Application Layers

The Java application is composed of several layers:
 - **controller**
 - **entities**
 - **repositories**
 - **services**
 - **validators**

## REST Service

This project is a REST Service to manage users. The endpoints are:

### GET: `/users`

Get all users

#### Parameters:

No parameters

##### Response:

**Code: 200**

```json
[
  {
    "id": 0,
    "firstname": "string",
    "lastname": "string",
    "birthdate": "string",
    "phoneNumber": "string",
    "email": "string",
    "createdAt": "string",
    "modifiedAt": "string"
  }
]
```

### POST: `/users`

Add a new user to the database.

#### Parameters:

**Body (object)**

| Name        | Type   | Description                                |
| ----------- | ------ | ------------------------------------------ |
| firstname*   | string | The firstname of the user.                |
| lastname*    | string | The lastname of the user.                 |
| birthdate*   | string | The birthdate of the user. Most be adult. <br> `Format: dd/MM/yyyy` |
| phoneNumber  | string | The phoneNumber of the user.              |
| email*       | string | The email of the user. <br> `Format: Email`                    |

***is required**

**Template**

```json
{
  "firstname": "string",
  "lastname": "string",
  "birthdate": "string",
  "phoneNumber": "string",
  "email": "string"
}
```

##### Response:

**Code: default**

No body returned for response

### GET: `/users/{id}`

Get users by user id

#### Parameters:

| Name  | Type          | Description                    |
| ----- | ------------- | ------------------------------ |
| id*   | string (path) | The id required to be fetched. |

***is required**

##### Response:

**Code: 200**

```json
{
  "id": 0,
  "firstname": "string",
  "lastname": "string",
  "birthdate": "string",
  "phoneNumber": "string",
  "email": "string",
  "createdAt": "string",
  "modifiedAt": "string"
}
```

**Code: 400**

```json
{
  "timestamp": "string",
  "status": 400,
  "error": "Bad Request",
  "path": "/users/string"
}
```

**Code: 404**

```json
{
  "timestamp": "string",
  "status": 404,
  "error": "Not Found",
  "path": "/users/string"
}
```

### DELETE: `/users/{id}`

Delete users by user id

#### Parameters:

| Name  | Type          | Description                    |
| ----- | ------------- | ------------------------------ |
| id*   | string (path) | The id required to be fetched. |

***is required**

##### Response:

**Code: 200**

No body returned for response

**Code: 400**

```json
{
  "timestamp": "string",
  "status": 400,
  "error": "Bad Request",
  "path": "/users/string"
}
```

**Code: 404**

```json
{
  "timestamp": "string",
  "status": 404,
  "error": "Not Found",
  "path": "/users/string"
}
```