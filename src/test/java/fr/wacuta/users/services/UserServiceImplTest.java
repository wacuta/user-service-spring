package fr.wacuta.users.services;

import fr.wacuta.users.entities.User;
import fr.wacuta.users.repositories.UsersRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {

    @Autowired
    private UsersServiceImpl usersService;

    private UsersRepository usersRepositoryMock;

    @Before
    public void initialized() {
        this.usersRepositoryMock = mock(UsersRepository.class, "UserRepository Mock");
        this.usersService.setUsersRepository(this.usersRepositoryMock);
    }

    @Test
    public void testGetAllUsers() {

        // Mock repository return
        List<User> response = new ArrayList<>();
        {
            User u = new User();
            u.setFirstname("Firstname 1");
            u.setLastname("Lastname 1");
            response.add(u);
        }
        {
            User u = new User();
            u.setFirstname("Firstname 2");
            u.setLastname("Lastname 2");
            response.add(u);
        }
        when(this.usersRepositoryMock.findAll()).thenReturn(response);

        // test
        List<User> test = this.usersService.getAllUsers();
        assertEquals("test response size", test.size(), 2);
        assertEquals("test user 1", test.get(0).getFirstname(), "Firstname 1");
        assertEquals("test user 2", test.get(1).getLastname(), "Lastname 2");

    }

    @Test
    public void testGetUserById() {

        final Integer userId = 1;

        // Mock repository return
        User user = new User();
        user.setFirstname("Firstname 1");
        user.setLastname("Lastname 1");
        when(this.usersRepositoryMock.findById(userId.toString()))
                .thenReturn(Optional.of(user));

        // test
        User test = this.usersService.getUserById(userId);
        assertEquals("test response user by id", test, user);

    }

    @Test(expected = ResponseStatusException.class)
    public void testGetUserById_NotFound() {

        final Integer userId = 1;

        // Mock repository return
        when(this.usersRepositoryMock.findById(userId.toString()))
                .thenThrow(NoSuchElementException.class);

        // test
        try {
            this.usersService.getUserById(userId);
        } catch (ResponseStatusException ex) {
            assertEquals("User get by id exception status test ", ex.getStatus(), HttpStatus.NOT_FOUND);
        }

        this.usersService.getUserById(userId);

    }

    @Test
    public void testAddUser() {

        // test argument
        ArgumentCaptor<User> arg = ArgumentCaptor.forClass(User.class);

        User user = new User();
        user.setFirstname("Firstname 1");
        user.setLastname("Lastname 1");

        this.usersService.addUser(user);

        verify(this.usersRepositoryMock, times(1)).save(arg.capture());
        assertEquals("test size argument", arg.getAllValues().size(), 1);
        User test = arg.getValue();
        assertEquals("test user save", test, user);

    }

    @Test
    public void testDeleteUserById() {

        final Integer userId = 1;

        // Mock repository return
        User user = new User();
        user.setFirstname("Firstname 1");
        user.setLastname("Lastname 1");
        when(this.usersRepositoryMock.findById(userId.toString()))
                .thenReturn(Optional.of(user));

        ArgumentCaptor<User> arg = ArgumentCaptor.forClass(User.class);

        // test
        this.usersService.deleteUserById(userId);

        verify(this.usersRepositoryMock, times(1)).delete(arg.capture());
        assertEquals("User delete test", arg.getValue(), user);
    }

    @Test(expected = ResponseStatusException.class)
    public void testDeleteUserById_NotFound() {

        final Integer userId = 1;

        // Mock repository return
        User user = new User();
        user.setFirstname("Firstname 1");
        user.setLastname("Lastname 1");
        when(this.usersRepositoryMock.findById(userId.toString()))
                .thenReturn(Optional.of(user));
        when(this.usersService.getUserById(userId)).thenThrow(NoSuchElementException.class);

        // test
        this.usersService.deleteUserById(userId);

    }
}
