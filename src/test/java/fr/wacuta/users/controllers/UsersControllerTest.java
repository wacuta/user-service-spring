package fr.wacuta.users.controllers;

import fr.wacuta.users.entities.User;
import fr.wacuta.users.services.UsersService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.server.ResponseStatusException;


import java.util.ArrayList;
import java.util.List;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UsersControllerTest {

    @Autowired
    private UsersController usersController;

    private UsersService usersService;

    @Before
    public void initialized() {
        this.usersService = mock(UsersService.class, "UserService Mock");
        this.usersController = new UsersController(this.usersService);
    }

    @Test
    public void testGetUsers() {

        List<User> response = new ArrayList<>();
        {
            User u = new User();
            u.setFirstname("Firstname 1");
            u.setLastname("Lastname 1");
            response.add(u);
        }
        {
            User u = new User();
            u.setFirstname("Firstname 2");
            u.setLastname("Lastname 2");
            response.add(u);
        }
        when(this.usersService.getAllUsers()).thenReturn(response);

        List<User> test = this.usersController.getUsers();

        assertEquals("test response size", test.size(), 2);
        assertEquals("test user 1", test.get(0).getFirstname(), "Firstname 1");
        assertEquals("test user 2", test.get(1).getLastname(), "Lastname 2");

    }

    @Test
    public void testGetUserById() {

        final Integer userId = 1;

        // Mock repository throw
        User user = new User();
        user.setFirstname("Firstname 1");
        user.setLastname("Lastname 1");
        when(this.usersService.getUserById(userId))
                .thenReturn(user);

        // test
        User test = this.usersController.getUserById(userId);
        assertEquals("test response user by id", test, user);

    }

    @Test(expected = ResponseStatusException.class)
    public void testGetUserById_NotFound() {

        final Integer userId = 1;

        // Mock repository return
        when(this.usersService.getUserById(userId))
                .thenThrow(ResponseStatusException.class);

        // test
        this.usersController.getUserById(userId);

    }

    @Test
    public void testAddUser() {

        // test argument
        ArgumentCaptor<User> arg = ArgumentCaptor.forClass(User.class);

        User user = new User();
        user.setFirstname("Firstname 1");
        user.setLastname("Lastname 1");

        this.usersService.addUser(user);

        verify(this.usersService, times(1)).addUser(arg.capture());
        assertEquals("test size argument", arg.getAllValues().size(), 1);
        User test = arg.getValue();
        assertEquals("test user save", test, user);

    }

    @Test
    public void testDeleteUserById() {

        final Integer userId = 1;

        // Mock repository return
        User user = new User();
        user.setFirstname("Firstname 1");
        user.setLastname("Lastname 1");
        when(this.usersService.getUserById(userId))
                .thenReturn(user);

        ArgumentCaptor<Integer> arg = ArgumentCaptor.forClass(Integer.class);

        // test
        this.usersService.deleteUserById(userId);

        verify(this.usersService, times(1)).deleteUserById(arg.capture());
        assertEquals("User delete test", arg.getValue(), userId);
    }

    @Test(expected = ResponseStatusException.class)
    public void testDeleteUserById_Notfound() {

        final Integer userId = 1;

        // Mock repository throw
        Mockito.doThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"))
                .doNothing()
                .when(this.usersService)
                .deleteUserById(userId);

        // test
        this.usersController.deleteUsers(userId);
    }
}
