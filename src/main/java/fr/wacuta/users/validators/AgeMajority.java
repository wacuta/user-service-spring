package fr.wacuta.users.validators;

import javax.validation.Constraint;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ FIELD })
@Retention(RUNTIME)
@Constraint(validatedBy = AgeMajorityValidator.class)
@Documented
public @interface AgeMajority {

    String message() default "Creation required legal age of majority";

    Class[] groups() default {};

    Class[] payload() default {};

    String dateFormat() default "dd/MM/yyy";
}
