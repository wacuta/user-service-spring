package fr.wacuta.users.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;

public class AgeMajorityValidator implements ConstraintValidator<AgeMajority, String> {

    String format;

    @Override
    public void initialize(AgeMajority constraintAnnotation) {
        format = constraintAnnotation.dateFormat();
    }

    /**
     * Main function to validate birthdate
     *  - it must be not null and not blank
     *  - the format must be respect
     *  - it must have legal age of majority
     *
     * @param date (String) user's birthdate
     * @param constraintValidatorContext validator's context
     * @return boolean if date is valid
     */
    @Override
    public boolean isValid(String date, ConstraintValidatorContext constraintValidatorContext) {

        if (date == null || "".equals(date)) {
            return false;
        }

        DateFormat dateFormat = new SimpleDateFormat( format );
        dateFormat.setLenient( false );

        try {
            LocalDate birthdate = dateFormat.parse(date)
                    .toInstant()
                    .atZone(ZoneId.systemDefault())
                    .toLocalDate();
            int age = Period.between(birthdate, LocalDate.now()).getYears();
            return age >= 18 && age < 110;
        } catch (ParseException e) {
            return false;
        }

    }
}
