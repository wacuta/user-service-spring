package fr.wacuta.users.controllers;

import fr.wacuta.users.entities.User;
import fr.wacuta.users.services.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * User Controller
 */
@Controller
@RequestMapping(value="/users")
public class UsersController {

    final static Logger LOG = LoggerFactory.getLogger(UsersController.class);

    private final UsersService usersService;

    public UsersController(final UsersService usersService) {
        this.usersService = usersService;
    }

    /**
     * Endpoint to get all users
     *
     * @return List of users
     */
    @GetMapping()
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public List<User> getUsers() {
        LOG.info("[GET] /users");
        return usersService.getAllUsers();
    }

    /**
     * Endpoint to get a user with its id
     *
     * @param userId (Integer) user's id
     * @return user or 404 not found
     */
    @GetMapping(value = "/{userId}")
    @ResponseBody
    @ResponseStatus(value = HttpStatus.OK)
    public User getUserById(@PathVariable Integer userId) {
        LOG.info("[GET] /users/" + userId);
        return usersService.getUserById(userId);
    }

    /**
     * Endpoint to create user
     * The user most be valid before saved
     *
     * @param newUser (User) the new user
     */
    @PostMapping()
    @ResponseBody
    @ResponseStatus(value = HttpStatus.CREATED)
    public void addUser(@Valid @RequestBody User newUser) {
        LOG.info("[POST] /users");
        usersService.addUser(newUser);
    }

    /**
     * Endpoint to delete user with its id
     *
     * @param userId (Integer) the user's id
     */
    @DeleteMapping(value = "/{userId}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteUsers(@PathVariable Integer userId) {
        LOG.info("[DELETE] /users/" + userId);
        usersService.deleteUserById(userId);
    }

}
