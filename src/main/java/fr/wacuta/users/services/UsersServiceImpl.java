package fr.wacuta.users.services;

import fr.wacuta.users.entities.User;
import fr.wacuta.users.repositories.UsersRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class UsersServiceImpl implements UsersService{

    final static Logger LOG = LoggerFactory.getLogger(UsersServiceImpl.class);

    private UsersRepository usersRepository;

    @Autowired
    public UsersServiceImpl(final UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public List<User> getAllUsers() {
        LOG.info("UsersService - get all users");
        return (List<User>) this.usersRepository.findAll();
    }

    @Override
    public User getUserById(Integer userId) throws ResponseStatusException {
        LOG.info("UsersService - Get users [id: " + userId + "]");
        try {
            return this.usersRepository.findById(userId.toString()).get();
        } catch (NoSuchElementException ex) {
            LOG.error("UsersService - Users [id: " + userId + "] Not found");
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }
    }

    @Override
    public void addUser(User user) {
        user.setCreatedAt(new Date());
        user.setModifiedAt(new Date());
        LOG.info("UsersService - Add " + user);
        this.usersRepository.save(user);
    }

    @Override
    public void deleteUserById(Integer userId) throws ResponseStatusException {
        User u = this.getUserById(userId);
        LOG.info("UsersService - Delete " + u);
        this.usersRepository.delete(u);
    }

    public void setUsersRepository(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }
}
