package fr.wacuta.users.services;

import fr.wacuta.users.entities.User;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

public interface UsersService {

    /**
     * Function to get all users
     *
     * @return List of User
     */
    List<User> getAllUsers();

    /**
     * Function to get a User with its id
     *
     * @param userId (Integer) the user's id
     *
     * @return the users
     * @throws ResponseStatusException if user is not in Database
     */
    User getUserById(Integer userId) throws ResponseStatusException;

    /**
     * Function to save a user un DB
     *
     * @param user the new User
     */
    void addUser(User user);

    /**
     * Function to delete User with its id
     *
     * @param userId (Integer) the user's id
     */
    void deleteUserById(Integer userId) throws ResponseStatusException;

}
